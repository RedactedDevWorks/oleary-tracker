<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Promise;
use AppBundle\Entity\RssLog;
use AppBundle\Event\PromiseFlaggedEvent;
use AppBundle\Event\PromiseNotificationEvent;
use AppBundle\Services\EmailListService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PromiseNotificationListener
{
    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var EmailListService
     */
    protected $emailer;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * PromiseNotificationListener constructor.
     * @param Registry $doctrine
     * @param EmailListService $emailer
     * @param Router $router
     * @param EngineInterface $templating
     */
    public function __construct(Registry $doctrine, EmailListService $emailer, Router $router, EngineInterface $templating, array $flag_map)
    {
        $this->doctrine = $doctrine;
        $this->emailer = $emailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->flag_map = $flag_map;
    }


    public function onNewPromiseNotification(PromiseNotificationEvent $event)
    {
        $em = $this->doctrine->getManager();

        $rsslog = new RssLog();
        $rsslog->setAction(RssLog::ACTION_CREATE)
            ->setTitle("New Promise: {$event->getPromise()->getTitle()}")
            ->setLink($this->router->generate('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$event->getPromise()->getUid()}")
            ->setDescription("A new promise has been created and entered into the database")
            ->setPromise($event->getPromise());
        $em->persist($rsslog);

        $em->flush();
    }

    public function onPromiseFlaggedNotification(PromiseFlaggedEvent $event)
    {
        $em = $this->doctrine->getManager();

        $reason = $this->flag_map[$event->getFlag()->reason];

        $rsslog = new RssLog();
        $rsslog->setAction(RssLog::ACTION_UPDATE)
            ->setTitle("Flagged: {$event->getPromise()->getTitle()}")
            ->setLink($this->router->generate('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$event->getPromise()->getUid()}")
            ->setDescription("This promise has been flagged by a user for reason: {$reason}")
            ->setPromise($event->getPromise());
        $em->persist($rsslog);

        $em->flush();

        $this->emailer->send($event->getPromise()->getWatchers(),
            'A promise you watch has been flagged',
            ':email:notification.promise.flagged.html.twig', [
                'promise' => $event->getPromise(),
                'reported_by' => $event->getFlag()->reported_by,
                'reason' => $reason
        ]);
    }

    public function onPromiseUpdatedTitleNotification(PromiseNotificationEvent $event)
    {
        $em = $this->doctrine->getManager();

        $rsslog = new RssLog();
        $rsslog->setAction(RssLog::ACTION_UPDATE)
            ->setTitle("Updated title: {$event->getPromise()->getTitle()}")
            ->setLink($this->router->generate('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$event->getPromise()->getUid()}")
            ->setDescription("This promise has an updated title")
            ->setPromise($event->getPromise());
        $em->persist($rsslog);

        $em->flush();

        $this->emailer->send($event->getPromise()->getWatchers(),
            'A promise you watch got an updated title',
            ':email:notification.promise.title.html.twig', [
                'promise' => $event->getPromise()
            ]);
    }

    public function onPromiseUpdatedDescriptionNotification(PromiseNotificationEvent $event)
    {
        $em = $this->doctrine->getManager();

        $rsslog = new RssLog();
        $rsslog->setAction(RssLog::ACTION_UPDATE)
            ->setTitle("Updated Description: {$event->getPromise()->getTitle()}")
            ->setLink($this->router->generate('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$event->getPromise()->getUid()}")
            ->setDescription("This promise has an updated description")
            ->setPromise($event->getPromise());
        $em->persist($rsslog);

        $em->flush();

        $this->emailer->send($event->getPromise()->getWatchers(),
            'A promise you watch had it\'s description updated',
            ':email:notification.promise.description.html.twig', [
                'promise' => $event->getPromise()
            ]);
    }

    public function onPromiseUpdatedStatusNotification(PromiseNotificationEvent $event) {
        $em = $this->doctrine->getManager();

        $rsslog = new RssLog();
        $rsslog->setAction(RssLog::ACTION_UPDATE)
            ->setTitle("{$event->getPromise()->getTitle()} is now '{$event->getStatus()}'")
            ->setLink($this->router->generate('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$event->getPromise()->getUid()}")
            ->setDescription("This promise has an updated status, to {$event->getStatus()} with the proof provided as {$event->getSource()}. Please help verify this source and status.")
            ->setPromise($event->getPromise());
        $em->persist($rsslog);

        $em->flush();

        $this->emailer->send($event->getPromise()->getWatchers(),
            "A promise you watch has been updated to {$event->getStatus()}",
            ':email:notification.promise.status.html.twig', [
                'promise' => $event->getPromise(),
                'status' => $event->getStatus(),
                'source' => $event->getSource()
            ]);
    }
}