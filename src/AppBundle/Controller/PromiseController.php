<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Promise;
use AppBundle\Entity\RssLog;
use AppBundle\Entity\Topic;
use AppBundle\Event\PromiseFlaggedEvent;
use AppBundle\Event\PromiseNotificationEvent;
use AppBundle\Form\PromiseFlag;
use AppBundle\Form\PromiseDescription;
use AppBundle\Form\PromiseEdit;
use AppBundle\Form\PromiseStatus;
use AppBundle\Form\PromiseTitle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PromiseController extends Controller
{
    /**
     * @Route("/promises", name="promises")
     */
    public function indexAction(Request $request)
    {
        $catRepo = $this->getDoctrine()->getRepository('AppBundle:PromiseCategory');
        $promiseRepo = $this->getDoctrine()->getRepository('AppBundle:Promise');

        $promise = new Promise();
        $create_form = $this->createForm(PromiseEdit::class, $promise);

        // @todo convert this to a model object
        $flag = new \stdClass();
        $flag->promise = null;
        $flag->reason = null;
        $flag->reported_by = null;
        $flag_form = $this->createForm(PromiseFlag::class, $flag, ['reason_choices'=>$this->getParameter('map_flags')]);

        $title = new \stdClass();
        $title->promise = null;
        $title->title = null;
        $title_form = $this->createForm(PromiseTitle::class, $title);

        $desc = new \stdClass();
        $desc->promise = null;
        $desc->description = null;
        $desc_form = $this->createForm(PromiseDescription::class, $desc);

        $status = new \stdClass();
        $status->promise = null;
        $status->status = null;
        $status->resolution = null;
        $status_form = $this->createForm(PromiseStatus::class, $status);

        $status_map = [
            'limbo' => Promise::STATUS_LIMBO,
            'progress' => Promise::STATUS_PROGRESS,
            'achieved' => Promise::STATUS_ACHIEVED,
            'broken' => Promise::STATUS_BROKEN
        ];

        if ($request->getMethod() == 'POST') {

            $em = $this->getDoctrine()->getManager();

            if($request->request->get('promise_edit')) {

                $create_form->handleRequest($request);
                if ($create_form->isValid()) {


                    if (!$promise->getId()) {
                        $promise->setAddedBy($this->getUser())
                            ->setDateAdded(new \DateTime())
                            ->setStatus(Promise::STATUS_LIMBO)
                            ->addContributor($this->getUser())
                            ->setUid(uniqid());

                        $forum = $this->getDoctrine()->getRepository('AppBundle:Forum')->findOneBy(['slug'=>'promises']);
                        $forum->setDateLastAction(new \DateTime());
                        $em->persist($forum);

                        $topic_uid = uniqid();
                        // create topic
                        $topic = new Topic();
                        $topic->setTitle($promise->getTitle())
                                ->setUid($topic_uid)
                                ->setForum($forum)
                                ->setAuthor($this->getUser())
                                ->setDateLastAction(new \DateTime())
                                ->setPromise($promise);

                        $em->persist($topic);

                        $post = new Post();
                        $post->setDateAdded(new \DateTime())
                            ->setTopic($topic)
                            ->setTitle($promise->getTitle())
                            ->setUid($topic_uid)
                            ->setTopic($topic)
                            ->setContent($promise->getDescription())
                            ->setAuthor($this->getUser());

                        $em->persist($post);

                        $promise->setTopic($topic);

                        $rsslog = new RssLog();
                        $rsslog->setAction(RssLog::ACTION_CREATE)
                            ->setTitle("New Promise: {$promise->getTitle()}")
                            ->setLink($this->generateUrl('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$post->getUid()}")
                            ->setDescription("A new promise has been created and entered into the database")
                            ->setPromise($promise);
                        $em->persist($rsslog);
                    }

                    $em->persist($promise);
                    $em->flush();

                    $this->addFlash('success', 'Successfully added promise to the database. Thank you for your contribution!');
                    return $this->redirectToRoute('promises');
                } else {
                    $this->addFlash('error', 'There was an issue adding this promise to the database. Please try again.');
                }

            } else if($request->request->get('promise_flag')) {

                $flag_form->handleRequest($request);
                if($flag_form->isValid()) {

                    if($this->getUser())
                        $flag->reported_by = [
                            'id' => $this->getUser()->getId(),
                            'email' => $this->getUser()->getEmail()
                        ];
                    else // log IP so we can detect spam/abuse
                        $flag->reported_by = [
                            'ip' => $request->getClientIp()
                        ];

                    $flagged_promise = $promiseRepo->find($flag->promise);

                    if($flagged_promise) {
                        $current_flags = $flagged_promise->getFlags();
                        if(!$current_flags) $current_flags = [];

                        $doFlag = true;
                        foreach($current_flags as $current_flag) {
                            $flag_author = $current_flag['reported_by'];

                            if(array_key_exists('email', $flag_author)) {
                                if($flag_author['email'] == $this->getUser()->getEmail()) {
                                    $doFlag = false;
                                }
                            } else {
                                if(array_key_exists('ip', $flag_author) && $flag_author['ip'] == $request->getClientIp()) {
                                    $doFlag = false;
                                }
                            }
                        }

                        if($doFlag) {
                            $current_flags[] = (array)$flag;
                            $flagged_promise->setFlags($current_flags);
                            $em->persist($flagged_promise);
                            $em->flush();

                            $this->addFlash('success', 'Successfully flagged this promise. Moderators will look into this as soon as possible. Thank you greatly for taking the time to let us know if imperfections in the database.');

                            $this->get('event_dispatcher')->dispatch('promise.notification.flagged', new PromiseFlaggedEvent($flagged_promise, $flag));

                        } else {
                            $this->addFlash('error', 'You have already flagged this item, please allow moderation times to handle the situation. Thank you for your continued interest in keeping our database in check.');
                        }

                        return $this->redirectToRoute('promises');
                    }
                }
            } else if($request->request->get('promise_title')) {

                $title_form->handleRequest($request);
                if($title_form->isValid()) {
                    $chosen_promise = $promiseRepo->find($title->promise);
                    if($chosen_promise) {
                        $chosen_promise->setTitle(filter_var($title->title, FILTER_SANITIZE_STRING))
                                        ->setDateUpdated(new \DateTime());

                        // we only want a person added once to this list, so lets try removing them if they already exist
                        $chosen_promise->removeContributor($this->getUser());
                        // to allow the add to just happen naturally
                        $chosen_promise->addContributor($this->getUser());

                        $em->persist($chosen_promise);

                        $rsslog = new RssLog();
                        $rsslog->setAction(RssLog::ACTION_UPDATE)
                            ->setTitle("Updated title: {$chosen_promise->getTitle()}")
                            ->setLink($this->generateUrl('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$chosen_promise->getUid()}")
                            ->setDescription("This promise has an updated title")
                            ->setPromise($chosen_promise);
                        $em->persist($rsslog);

                        $em->flush();
                        $this->addFlash('success', 'Title updated successfully! Thank you');
                        $this->get('event_dispatcher')->dispatch('promise.notification.update.title', new PromiseNotificationEvent($chosen_promise));

                        return $this->redirectToRoute('promises');
                    } else {
                        $this->addFlash('error', 'This promise couldn\'t be found in the database, please try again');
                    }
                } else {
                    $this->addFlash('error', 'Something about the form didn\'t validate correctly. Please try again.');
                }
            } else if($request->request->get('promise_description')) {

                $desc_form->handleRequest($request);
                if($desc_form->isValid()) {
                    $chosen_promise = $promiseRepo->find($desc->promise);
                    if($chosen_promise) {
                        $chosen_promise->setDescription(filter_var($desc->description, FILTER_SANITIZE_STRING))
                                        ->setDateUpdated(new \DateTime());

                        // we only want a person added once to this list, so lets try removing them if they already exist
                        $chosen_promise->removeContributor($this->getUser());
                        // to allow the add to just happen naturally
                        $chosen_promise->addContributor($this->getUser());

                        $em->persist($chosen_promise);
                        $em->flush();

                        $rsslog = new RssLog();
                        $rsslog->setAction(RssLog::ACTION_UPDATE)
                            ->setTitle("Updated Description: {$chosen_promise->getTitle()}")
                            ->setLink($this->generateUrl('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$chosen_promise->getUid()}")
                            ->setDescription("This promise has an updated description")
                            ->setPromise($chosen_promise);
                        $em->persist($rsslog);
                        $em->flush();

                        $this->addFlash('success', 'Description updated successfully! Thank you');
                        $this->get('event_dispatcher')->dispatch('promise.notification.update.description', new PromiseNotificationEvent($chosen_promise));
                        return $this->redirectToRoute('promises');
                    } else {
                        $this->addFlash('error', 'This promise couldn\'t be found in the database, please try again');
                    }
                } else {
                    $this->addFlash('error', 'Something about the form didn\'t validate correctly. Please try again.');
                }
            } else if($request->request->get('promise_status')) {

                $status_form->handleRequest($request);
                if($status_form->isValid()) {

                    $chosen_promise = $promiseRepo->find($status->promise);
                    if($chosen_promise && array_key_exists($status->status, $status_map)) {

                        $chosen_promise->setStatus($status_map[$status->status])
                                       ->setDateUpdated(new \DateTime());
                        $sources = $chosen_promise->getResolutionSources();
                        if(!$sources) $sources = [];
                        $sources[$status->status] = $status->resolution;

                        $chosen_promise->setResolutionSources($sources);

                        // we only want a person added once to this list, so lets try removing them if they already exist
                        $chosen_promise->removeContributor($this->getUser());
                        // to allow the add to just happen naturally
                        $chosen_promise->addContributor($this->getUser());

                        $em->persist($chosen_promise);

                        $rsslog = new RssLog();
                        $rsslog->setAction(RssLog::ACTION_UPDATE)
                            ->setTitle("{$chosen_promise->getTitle()} is now '{$status->status}'")
                            ->setLink($this->generateUrl('promises', [], UrlGeneratorInterface::ABSOLUTE_URL)."#{$chosen_promise->getUid()}")
                            ->setDescription("This promise has an updated status, to {$status->status} with the proof provided as {$status->resolution}. Please help verify this source and status.")
                            ->setPromise($chosen_promise);
                        $em->persist($rsslog);


                        $em->flush();
                        $this->addFlash('success', 'Status updated successfully! Thank you');
                        $this->get('event_dispatcher')->dispatch('promise.notification.update.status', new PromiseNotificationEvent($chosen_promise, $status->status, $status->resolution));
                        return $this->redirectToRoute('promises');

                    } else {
                        $this->addFlash('error', 'This promise couldn\'t be found in the database, please try again');
                    }

                } else {
                    $this->addFlash('error', 'Something about the form didn\'t validate correctly. Please try again.');
                }
            }
        }

        // Get filter
        $filter = $request->query->filter('filter', null, FILTER_SANITIZE_STRING);
        if($filter)
            if (array_key_exists($filter, $status_map))
                $filter = $status_map[$filter];
            else
                $filter = null;

        $search = $request->query->filter('q', null, FILTER_SANITIZE_STRING);
        $searchCategories = $request->query->filter('category', 'all', FILTER_SANITIZE_STRING);
        if('all' == $searchCategories)
          $searchCategories = '';

        // Page data
        $forUser = $request->query->get('favorites', null) ? $this->getUser() : null;
        $categories = $catRepo->findPopulatedCategories($filter, $search, $searchCategories, $forUser);

        // Promise Counter
        $counts = $promiseRepo->getCounts($search);

        // replace this example code with whatever you need
        return $this->render('promises/index.html.twig', [
            'page_title' => 'Promises',
            'categories' => $categories,
            'counts' => $counts,
            'create_form' => $create_form->createView(),
            'flag_form' => $flag_form->createView(),
            'title_form' => $title_form->createView(),
            'desc_form' => $desc_form->createView(),
            'status_form' => $status_form->createView()
        ]);
    }

    /**
     * @Route("/promises/watch/{promise}", name="promise_watch")
     * @Route("/promises/unwatch/{promise}", name="promise_unwatch")
     */
    public function watchToggleAction(Request $request, Promise $promise) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        switch($request->get('_route')) {
            case "promise_watch":
                $user->addWatchPromise($promise);
                $this->addFlash('success', 'You have successfully subscribed to this issue. Check out your personal RSS feed for updates!');
                break;
            case "promise_unwatch":
                $user->removeWatchPromise($promise);
                $this->addFlash('success', 'You have successfully unsubscribed to this issue. It will no longer appear in your personal feeds.');
                break;
        }

        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('promises');
    }


    // ajaxed components

    /**
     * @Route("/modal/promise/edit/description/{promise}", name="modal_promise_edit_description")
     */
    public function modalEditDescription(Request $request, Promise $promise = null)
    {
        if(!$promise) $promise = new Promise();
        $desc_form = $this->createForm(PromiseDescription::class, $promise);

        return $this->render('promises/modal.edit.description.html.twig', [
            'modal_title' => 'Edit Description',
            'desc_form' => $desc_form->createView(),
        ]);
    }

    /**
     * @Route("/modal/promise/{promise}", name="modal_promise")
     */
    public function modalPromise(Request $request, Promise $promise = null)
    {
        return $this->render('promises/modal.promise.html.twig', [
            'modal_title' => $promise->getTitle(),
            'promise' => $promise
        ]);
    }
}
