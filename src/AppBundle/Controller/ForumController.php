<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Forum;
use AppBundle\Entity\Post;
use AppBundle\Entity\Topic;
use AppBundle\Form\ForumReply;
use AppBundle\Form\TopicEdit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ForumController extends Controller
{
    /**
     * @Route("/discussion", name="forum_index")
     */
    public function indexAction()
    {
        $forumRepo = $this->getDoctrine()->getRepository('AppBundle:Forum');
        $forums = $forumRepo->findBy([], ['sort'=>'ASC']);
        return $this->render('forum/index.html.twig', [
            'page_title' => 'Discuss',
            'forums' => $forums
        ]);
    }

    /**
     * @Route("/discussion/{slug}", name="forum")
     */
    public function forumAction(Request $request, Forum $forum)
    {
        $topicRepo = $this->getDoctrine()->getRepository('AppBundle:Topic');
        $topics = $topicRepo->findTopicsPage($forum);

        $topicModel = new \stdClass();
        $topicModel->title = null;
        $topicModel->content = null;

        $topicForm = $this->createForm(TopicEdit::class, $topicModel);

        if($request->getMethod() == 'POST') {
            $topicForm->handleRequest($request);
            if($topicForm->isValid()) {
                $topic = new Topic();
                $post = new Post();

                $uid = uniqid();

                $topic->setTitle($topicModel->title)
                      ->setUid($uid)
                      ->setAuthor($this->getUser())
                      ->setDateLastAction(new \DateTime())
                      ->setForum($forum);

                $post->setTitle($topicModel->title)
                    ->setUid($uid)
                    ->setAuthor($this->getUser())
                    ->setContent($topicModel->content)
                    ->setDateAdded(new \DateTime())
                    ->setDateUpdated(new \DateTime())
                    ->setTopic($topic);

                $em = $this->getDoctrine()->getManager();
                $em->persist($topic);
                $em->persist($post);
                $em->flush();

                if($topic->getId() && $post->getId()) {
                    $this->addFlash('success', 'Successfully created new topic');
                    return $this->redirectToRoute('topic', ['forum_slug'=>$forum->getSlug(), 'topic_slug'=>$topic->getSlug()]);
                } else {
                    $this->addFlash('error', 'There was an issue posting your topic. Please try again');
                }
            }
        }

        return $this->render('forum/forum.html.twig', [
            'page_title' => $forum->getTitle() . ' | Discuss',
            'forum' => $forum,
            'topics' => $topics,
            'create_form' => $topicForm->createView()
        ]);
    }

    /**
     * @Route("/discussion/{forum_slug}/{topic_slug}", name="topic")
     * @ParamConverter("forum", options={"mapping": {"forum_slug": "slug"}})
     * @ParamConverter("topic", options={"mapping": {"topic_slug": "slug"}})
     */
    public function topicAction(Request $request, Forum $forum, Topic $topic)
    {
        $postRepo = $this->getDoctrine()->getRepository('AppBundle:Post');
        $page = $request->query->filter('page', 1, FILTER_SANITIZE_NUMBER_INT);

        $limit = 10;
        $offset = ($page - 1) * $limit;
        if($offset < 0) $offset = 0;

        $post = new Post();
        $form = $this->createForm(ForumReply::class, $post);

        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if($form->isValid()) {

                $post->setTopic($topic)
                     ->setUid(uniqid())
                     ->setAuthor($this->getUser())
                     ->setDateAdded(new \DateTime());

                $topic->setDateLastAction(new \DateTime());
                $forum->setDateLastAction(new \DateTime());

                $em = $this->getDoctrine()->getManager();
                $em->persist($post);
                $em->persist($topic);
                $em->persist($forum);
                $em->flush();

                return $this->redirectToRoute('topic', ['forum_slug'=>$forum->getSlug(), 'topic_slug'=>$topic->getSlug()]);
            }
        }

        $posts = $postRepo->findPostsByTopic($topic, $offset, $limit);

        return $this->render('forum/topic.html.twig', [
            'page_title' => $topic->getTitle() . ' | Discuss',
            'forum' => $forum,
            'topic' => $topic,
            'posts' => $posts,
            'page' => $page,
            'form' => $form->createView()
        ]);
    }
  
  /**
  * @Route("/discussion/{forum}/topic/new", name="forum_topic_new")
  */
  public function createTopicAction(Request $request, Forum $forum) 
  {

  }
}
