<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ComponentController
 * @package AppBundle\Controller
 * @Route("/component")
 */
class ComponentController extends Controller
{

    /**
     * @Route("/search", name="component_search")
     */
    public function searchComponent(Request $request)
    {
        $cats = $this->getDoctrine()->getRepository('AppBundle:PromiseCategory')->findBy([], ['title'=>'asc']);
        
        $stack = $this->get('request_stack');
        $masterRequest = $stack->getMasterRequest();
      
        return $this->render(':components:search.html.twig', [
            'categories' => $cats,
            'master_request' => $masterRequest
        ]);

    }

}