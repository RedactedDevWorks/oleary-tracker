<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Form\SecurityRegister;
use AppBundle\Model\AccountPreference;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction() {}

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request)
    {
        $formdata = new Account();
        $form = $this->createForm(SecurityRegister::class, $formdata);

        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if($this->getDoctrine()->getRepository('AppBundle:Account')->findOneBy(['email'=>$formdata->getEmail()])) {
                $this->addFlash('error', 'This email is already registered into the system.');
                return $this->redirectToRoute('register');
            }

            $preferences = new AccountPreference();
            $preferences->setEmailWatchedPromises(true);
            $preferences->setEmailWatchedTopics(true);

            $formdata->setRoles(['ROLE_USER'])
                     ->setDateAdded(new \DateTime())
                     ->setPassword(password_hash($formdata->getPassword(), PASSWORD_BCRYPT))
                     ->setPreferences((object)$preferences)
                     ->setPasswordToken(uniqid('pw'))
                     ->setIsActive(false);

            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($formdata);
                $em->flush();

                $this->get('app.emailer')->send([$formdata], 'Validate your email address', ':email:account.validate.email.html.twig', []);

                return $this->redirectToRoute('homepage');
            } else {
                $this->addFlash('error', $form->getErrors(true, true));
            }
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/validate/{passwordToken}", name="validate")
     */
    public function validateAction(Request $request, Account $account)
    {
        $em = $this->getDoctrine()->getManager();
        if(!$account->getIsActive()) {
            $account->setIsActive(true);
            $em->persist($account);
            $em->flush();
            $this->addFlash('success', 'Email has been successfully validated, please login to continue.');
            return $this->redirectToRoute('login');
        }

        $this->addFlash('error', 'Email has already been validated');
        return $this->redirectToRoute('homepage');
    }
}
