<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FeedController extends Controller
{
    /**
     * @Route("/rss.xml", name="main_rss", defaults={"_format": "xml"})
     */
    public function mainFeed()
    {
        $feed = $this->getDoctrine()->getRepository('AppBundle:RssLog')
                     ->findBy([], ['dateAdded'=>'DESC'], 20);

        return $this->render('::feed.xml.twig', [
            'feed_title' => 'O\'Leary Promise Tracker Feed',
            'feed_description' => 'A feed for tracking updates to Kevin O\'Leary\'s promises through his political career',
            'feed_date' => new \DateTime(),
            'feed_lang' => 'en-ca',
            'feed_link' => $this->generateUrl('homepage', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'feed_ttl' => 7200,
            'feed' => $feed
        ]);
    }
  
    /**
    * @Route("/rss/{token}.xml", name="custom_feed", defaults={"_format": "xml"})
    */
    public function customFeedAction($token)
    {
        $decoded = base64_decode($token);
        $account = $this->getDoctrine()->getRepository('AppBundle:Account')
                        ->findOneBy(['email'=>$decoded]);

        $feed = $this->getDoctrine()->getRepository('AppBundle:RssLog')
                     ->findBy(['promise'=>$account->getWatchPromise()->toArray()], ['dateAdded'=>'DESC'], 20);

        return $this->render('::feed.xml.twig', [
            'feed_title' => $account->getDisplayName() . '\'s Custom O\'Leary Promise Tracker Feed',
            'feed_description' => 'A custom watch feed for tracking updates to Kevin O\'Leary\'s promises through his political career',
            'feed_date' => new \DateTime(),
            'feed_lang' => 'en-ca',
            'feed_link' => $this->generateUrl('homepage', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'feed_ttl' => 7200,
            'feed' => $feed
        ]);
    }

}