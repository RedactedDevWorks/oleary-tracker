<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $promiseRepo = $this->getDoctrine()->getRepository('AppBundle:Promise');
        $counts = $promiseRepo->getCounts();

        return $this->render('default/index.html.twig', [
            'counts' => $counts,
        ]);
    }
}
