<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Form\EventEdit;
use AppBundle\Entity\Event;

class EventsController extends Controller
{
    /**
     * @Route("/events", name="events")
     */
    public function indexAction()
    {
        $timeline = $this->getDoctrine()->getRepository('AppBundle:Event')
                                        ->findBy([], ['dateAdded'=>'DESC']);
      
        $event = new Event();
        $form = $this->createForm(EventEdit::class, $event);
      
        dump($timeline);

        return $this->render('::events.html.twig', [
            'events' => $timeline,
            'create_form' => $form->createView()
        ]);
    }
}