<?php

namespace AppBundle\Controller;

use AppBundle\Form\ProfileSettingsEdit;
use AppBundle\Model\AccountPreference;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends Controller
{
    /**
     * @Route("/profile/settings", name="profile_settings")
     * @Security("has_role('ROLE_USER')")
     */
    public function settingsAction(Request $request)
    {
        $preferences = $this->getUser()->getPreferences();
        if (!$preferences) $preferences = new AccountPreference();

        $form = $this->createForm(ProfileSettingsEdit::class, $preferences);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $user = $this->getDoctrine()->getRepository('AppBundle:Account')->find($this->getUser()->getId());

                $user->setPreferences(clone $preferences)->setDateUpdated(new \DateTime());

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Successfully update profile');
                return $this->redirectToRoute('profile_settings');
            } else {
                $this->addFlash('error', 'Please correct some issues with this form');
            }
        }

        return $this->render('profile/settings.html.twig', [
            'form' => $form->createView()
        ]);
    }
      
      
      
    /**
    * @Route("/profile/favorites", name="profile_favorites")
    */
    public function favoritesAction(Request $request) {
        $accountRepo = $this->getDoctrine()->getRepository("AppBundle:Account");
        $topicRepo = $this->getDoctrine()->getRepository("AppBundle:Topic");
        $promiseRepo = $this->getDoctrine()->getRepository("AppBundle:Promise");

        $favorites = [
            "topics" => $topicRepo->getFavorites($this->getUser()),
            "promises" => $promiseRepo->getFavorites($this->getUser())
        ];

        dump($favorites);

        return $this->render("profile/favorites.html.twig", [
            'page_title' => 'Manage Favorites',
            'favorites' => $favorites
        ]);
    }
}
