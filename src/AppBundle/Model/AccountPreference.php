<?php

namespace AppBundle\Model;

class AccountPreference
{
    /**
     * @var boolean
     */
    public $email_watched_promises;
    /**
     * @var boolean
     */
    public $email_watched_topics;

    /**
     * @return boolean
     */
    public function getEmailWatchedPromises()
    {
        return $this->email_watched_promises;
    }

    /**
     * @param boolean $email_watched_promises
     */
    public function setEmailWatchedPromises($email_watched_promises)
    {
        $this->email_watched_promises = $email_watched_promises;
    }

    /**
     * @return boolean
     */
    public function getEmailWatchedTopics()
    {
        return $this->email_watched_topics;
    }

    /**
     * @param boolean $email_watched_topics
     */
    public function setEmailWatchedTopics($email_watched_topics)
    {
        $this->email_watched_topics = $email_watched_topics;
    }


}