<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Account
 *
 * @ORM\Table(name="account")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountRepository")
 */
class Account implements AdvancedUserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=255, nullable=true)
     */
    private $displayName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime")
     */
    private $dateAdded;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="simple_array", nullable=false)
     */
    private $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="password_token", type="string", length=255, nullable=true)
     */
    private $passwordToken;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_token", type="string", length=255, nullable=true, unique=true)
     */
    private $facebookToken;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_token", type="string", length=255, nullable=true, unique=true)
     */
    private $twitterToken;

    /**
     * @var string
     *
     * @ORM\Column(name="google_token", type="string", length=255, nullable=true, unique=true)
     */
    private $googleToken;

    /**
     * @var string
     *
     * @ORM\Column(name="github_token", type="string", length=255, nullable=true, unique=true)
     */
    private $githubToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_last_login", type="datetime", nullable=true)
     */
    private $dateLastLogin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_updated", type="datetime", nullable=true)
     */
    private $dateUpdated;

    /**
     * @var array
     *
     * @ORM\Column(name="preferences", type="object", nullable=true)
     */
    private $preferences;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Promise", inversedBy="watchers")
     * @ORM\JoinTable(name="account_watch_promise")
     */
    private $watchPromise;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Topic")
     * @ORM\JoinTable(name="account_watch_topic")
     */
    private $watchTopic;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="author", fetch="EXTRA_LAZY")
     */
    private $posts;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;



    public function getUsername()
    {
        return $this->getEmail();
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }


    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
//        $this->password = null;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->isActive
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->isActive
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    // ...

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->watchPromise = new \Doctrine\Common\Collections\ArrayCollection();
        $this->watchTopic = new \Doctrine\Common\Collections\ArrayCollection();
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Account
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return Account
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Account
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Account
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return Account
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Set passwordToken
     *
     * @param string $passwordToken
     *
     * @return Account
     */
    public function setPasswordToken($passwordToken)
    {
        $this->passwordToken = $passwordToken;

        return $this;
    }

    /**
     * Get passwordToken
     *
     * @return string
     */
    public function getPasswordToken()
    {
        return $this->passwordToken;
    }

    /**
     * Set facebookToken
     *
     * @param string $facebookToken
     *
     * @return Account
     */
    public function setFacebookToken($facebookToken)
    {
        $this->facebookToken = $facebookToken;

        return $this;
    }

    /**
     * Get facebookToken
     *
     * @return string
     */
    public function getFacebookToken()
    {
        return $this->facebookToken;
    }

    /**
     * Set twitterToken
     *
     * @param string $twitterToken
     *
     * @return Account
     */
    public function setTwitterToken($twitterToken)
    {
        $this->twitterToken = $twitterToken;

        return $this;
    }

    /**
     * Get twitterToken
     *
     * @return string
     */
    public function getTwitterToken()
    {
        return $this->twitterToken;
    }

    /**
     * Set googleToken
     *
     * @param string $googleToken
     *
     * @return Account
     */
    public function setGoogleToken($googleToken)
    {
        $this->googleToken = $googleToken;

        return $this;
    }

    /**
     * Get googleToken
     *
     * @return string
     */
    public function getGoogleToken()
    {
        return $this->googleToken;
    }

    /**
     * Set githubToken
     *
     * @param string $githubToken
     *
     * @return Account
     */
    public function setGithubToken($githubToken)
    {
        $this->githubToken = $githubToken;

        return $this;
    }

    /**
     * Get githubToken
     *
     * @return string
     */
    public function getGithubToken()
    {
        return $this->githubToken;
    }

    /**
     * Set dateLastLogin
     *
     * @param \DateTime $dateLastLogin
     *
     * @return Account
     */
    public function setDateLastLogin($dateLastLogin)
    {
        $this->dateLastLogin = $dateLastLogin;

        return $this;
    }

    /**
     * Get dateLastLogin
     *
     * @return \DateTime
     */
    public function getDateLastLogin()
    {
        return $this->dateLastLogin;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Account
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set preferences
     *
     * @param \stdClass $preferences
     *
     * @return Account
     */
    public function setPreferences($preferences)
    {
        $this->preferences = $preferences;

        return $this;
    }

    /**
     * Get preferences
     *
     * @return \stdClass
     */
    public function getPreferences()
    {
        return $this->preferences;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Account
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add watchPromise
     *
     * @param \AppBundle\Entity\Promise $watchPromise
     *
     * @return Account
     */
    public function addWatchPromise(\AppBundle\Entity\Promise $watchPromise)
    {
        $this->watchPromise[] = $watchPromise;

        return $this;
    }

    /**
     * Remove watchPromise
     *
     * @param \AppBundle\Entity\Promise $watchPromise
     */
    public function removeWatchPromise(\AppBundle\Entity\Promise $watchPromise)
    {
        $this->watchPromise->removeElement($watchPromise);
    }

    /**
     * Get watchPromise
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWatchPromise()
    {
        return $this->watchPromise;
    }

    /**
     * Add watchTopic
     *
     * @param \AppBundle\Entity\Topic $watchTopic
     *
     * @return Account
     */
    public function addWatchTopic(\AppBundle\Entity\Topic $watchTopic)
    {
        $this->watchTopic[] = $watchTopic;

        return $this;
    }

    /**
     * Remove watchTopic
     *
     * @param \AppBundle\Entity\Topic $watchTopic
     */
    public function removeWatchTopic(\AppBundle\Entity\Topic $watchTopic)
    {
        $this->watchTopic->removeElement($watchTopic);
    }

    /**
     * Get watchTopic
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWatchTopic()
    {
        return $this->watchTopic;
    }

    /**
     * Add post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return Account
     */
    public function addPost(\AppBundle\Entity\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \AppBundle\Entity\Post $post
     */
    public function removePost(\AppBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }
}
