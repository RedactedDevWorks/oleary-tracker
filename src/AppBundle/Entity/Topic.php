<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Topic
 *
 * @ORM\Table(name="topic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TopicRepository")
 */
class Topic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Forum
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Forum", inversedBy="topics")
     */
    private $forum;

    /**
     * @var Account
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Account")
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=255)
     */
    private $uid;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     * @Gedmo\Slug(fields={"uid", "title"}, unique=true, updatable=false)
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var Post
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="topic")
     */
    private $posts;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_last_action", type="datetime")
     */
    private $dateLastAction;

    /**
     * @var Promise
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Promise", inversedBy="topic", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true)
     */
    private $promise;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set forum
     *
     * @param \AppBundle\Entity\Forum $forum
     *
     * @return Topic
     */
    public function setForum(\AppBundle\Entity\Forum $forum = null)
    {
        $this->forum = $forum;

        return $this;
    }

    /**
     * Get forum
     *
     * @return \AppBundle\Entity\Forum
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * Add post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return Topic
     */
    public function addPost(\AppBundle\Entity\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \AppBundle\Entity\Post $post
     */
    public function removePost(\AppBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set dateLastAction
     *
     * @param \DateTime $dateLastAction
     *
     * @return Topic
     */
    public function setDateLastAction($dateLastAction)
    {
        $this->dateLastAction = $dateLastAction;

        return $this;
    }

    /**
     * Get dateLastAction
     *
     * @return \DateTime
     */
    public function getDateLastAction()
    {
        return $this->dateLastAction;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Topic
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Topic
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return Topic
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set promise
     *
     * @param \AppBundle\Entity\Promise $promise
     *
     * @return Topic
     */
    public function setPromise(\AppBundle\Entity\Promise $promise = null)
    {
        $this->promise = $promise;

        return $this;
    }

    /**
     * Get promise
     *
     * @return \AppBundle\Entity\Promise
     */
    public function getPromise()
    {
        return $this->promise;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\Account $author
     *
     * @return Topic
     */
    public function setAuthor(\AppBundle\Entity\Account $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
