<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Promise
 *
 * @ORM\Table(name="promise_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PromiseCategoryRepository")
 */
class PromiseCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="title", type="string", length=50)
     */
    private $title;
  
    /**
    * @var string
    * @Gedmo\Slug(fields={"title"}, unique=true, updatable=false)
    * @ORM\Column(name="slug", type="string", length=255, nullable=false)
    */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Account")
     * @ORM\JoinColumn(name="added_by")
     */
    private $addedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime")
     */
    private $dateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_updated", type="datetime")
     *
     */
    private $dateUpdated;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Promise", mappedBy="category")
     */
    private $promises;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PromiseCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return PromiseCategory
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PromiseCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return PromiseCategory
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return PromiseCategory
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set addedBy
     *
     * @param \AppBundle\Entity\Account $addedBy
     *
     * @return PromiseCategory
     */
    public function setAddedBy(\AppBundle\Entity\Account $addedBy = null)
    {
        $this->addedBy = $addedBy;

        return $this;
    }

    /**
     * Get addedBy
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->promises = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add promise
     *
     * @param \AppBundle\Entity\Promise $promise
     *
     * @return PromiseCategory
     */
    public function addPromise(\AppBundle\Entity\Promise $promise)
    {
        $this->promises[] = $promise;

        return $this;
    }

    /**
     * Remove promise
     *
     * @param \AppBundle\Entity\Promise $promise
     */
    public function removePromise(\AppBundle\Entity\Promise $promise)
    {
        $this->promises->removeElement($promise);
    }

    /**
     * Get promises
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPromises()
    {
        return $this->promises;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return PromiseCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
