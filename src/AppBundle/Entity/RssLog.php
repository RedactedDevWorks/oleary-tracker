<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Topic
 *
 * @ORM\Table(name="rss_log")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RssLogRepository")
 */
class RssLog
{
    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;
    const ACTION_REMOVED = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $action;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=256)
     */
    private $link;

    /**
     * @var string
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $dateAdded;
  
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Promise")
    * @ORM\JoinColumn()
    */
    private $promise;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param integer $action
     *
     * @return RssLog
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return integer
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return RssLog
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return RssLog
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return RssLog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return RssLog
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }
  
  

    /**
     * Set promise
     *
     * @param \AppBundle\Entity\Promise $promise
     *
     * @return RssLog
     */
    public function setPromise(\AppBundle\Entity\Promise $promise = null)
    {
        $this->promise = $promise;

        return $this;
    }

    /**
     * Get promise
     *
     * @return \AppBundle\Entity\Promise
     */
    public function getPromise()
    {
        return $this->promise;
    }
}
