<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Promise
 *
 * @ORM\Table(name="promise")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PromiseRepository")
 */
class Promise
{
    const STATUS_LIMBO = 1;
    const STATUS_PROGRESS = 2;
    const STATUS_ACHIEVED = 3;
    const STATUS_BROKEN = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $uid;

    /**
     * @var PromiseCategory
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PromiseCategory", inversedBy="promises")
     * @ORM\JoinColumn(name="category")
     */
    private $category;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Account")
     * @ORM\JoinColumn(name="added_by")
     */
    private $addedBy;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Account")
     * @ORM\JoinTable(name="promise_contributors")
     */
    private $contributors;

    /**
     * @var Topic
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Topic", cascade={"persist"}, mappedBy="promise")
     */
    private $topic;

    /**
     * @var string
     *
     * @ORM\Column(name="source_link", type="string", length=255, unique=true)
     */
    private $sourceLink;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_sources", type="simple_array", nullable=true)
     */
    private $additionalSources;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2048, nullable=true)
     */
    private $description;

    /**
     * @var array
     *
     * @ORM\Column(name="resolution_sources", type="json_array", nullable=true)
     */
    private $resolutionSources;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime")
     */
    private $dateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_promised", type="date")
     */
    private $datePromised;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_updated", type="datetime", nullable=true)
     */
    private $dateUpdated;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var array
     *
     * @ORM\Column(name="flags", type="json_array", nullable=true)
     */
    private $flags;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Account", mappedBy="watchPromise")
     */
    private $watchers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contributors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sourceLink
     *
     * @param string $sourceLink
     *
     * @return Promise
     */
    public function setSourceLink($sourceLink)
    {
        $this->sourceLink = $sourceLink;

        return $this;
    }

    /**
     * Get sourceLink
     *
     * @return string
     */
    public function getSourceLink()
    {
        return $this->sourceLink;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Promise
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set additionalSources
     *
     * @param array $additionalSources
     *
     * @return Promise
     */
    public function setAdditionalSources($additionalSources)
    {
        $this->additionalSources = $additionalSources;

        return $this;
    }

    /**
     * Get additionalSources
     *
     * @return array
     */
    public function getAdditionalSources()
    {
        return $this->additionalSources;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Promise
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set resolutionSources
     *
     * @param array $resolutionSources
     *
     * @return Promise
     */
    public function setResolutionSources($resolutionSources)
    {
        $this->resolutionSources = $resolutionSources;

        return $this;
    }

    /**
     * Get resolutionSources
     *
     * @return array
     */
    public function getResolutionSources()
    {
        return $this->resolutionSources;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Promise
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set datePromised
     *
     * @param \DateTime $datePromised
     *
     * @return Promise
     */
    public function setDatePromised($datePromised)
    {
        $this->datePromised = $datePromised;

        return $this;
    }

    /**
     * Get datePromised
     *
     * @return \DateTime
     */
    public function getDatePromised()
    {
        return $this->datePromised;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Promise
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set addedBy
     *
     * @param \AppBundle\Entity\Account $addedBy
     *
     * @return Promise
     */
    public function setAddedBy(\AppBundle\Entity\Account $addedBy = null)
    {
        $this->addedBy = $addedBy;

        return $this;
    }

    /**
     * Get addedBy
     *
     * @return \AppBundle\Entity\Account
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }

    /**
     * Add contributor
     *
     * @param \AppBundle\Entity\Account $contributor
     *
     * @return Promise
     */
    public function addContributor(\AppBundle\Entity\Account $contributor)
    {
        $this->contributors[] = $contributor;

        return $this;
    }

    /**
     * Remove contributor
     *
     * @param \AppBundle\Entity\Account $contributor
     */
    public function removeContributor(\AppBundle\Entity\Account $contributor)
    {
        $this->contributors->removeElement($contributor);
    }

    /**
     * Get contributors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContributors()
    {
        return $this->contributors;
    }

    /**
     * Set topic
     *
     * @param \AppBundle\Entity\Topic $topic
     *
     * @return Promise
     */
    public function setTopic(\AppBundle\Entity\Topic $topic = null)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return \AppBundle\Entity\Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Promise
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\PromiseCategory $category
     *
     * @return Promise
     */
    public function setCategory(\AppBundle\Entity\PromiseCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\PromiseCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set flags
     *
     * @param array $flags
     *
     * @return Promise
     */
    public function setFlags($flags)
    {
        $this->flags = $flags;

        return $this;
    }

    /**
     * Get flags
     *
     * @return array
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return Promise
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Add watcher
     *
     * @param \AppBundle\Entity\Account $watcher
     *
     * @return Promise
     */
    public function addWatcher(\AppBundle\Entity\Account $watcher)
    {
        $this->watchers[] = $watcher;

        return $this;
    }

    /**
     * Remove watcher
     *
     * @param \AppBundle\Entity\Account $watcher
     */
    public function removeWatcher(\AppBundle\Entity\Account $watcher)
    {
        $this->watchers->removeElement($watcher);
    }

    /**
     * Get watchers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWatchers()
    {
        return $this->watchers;
    }
}
