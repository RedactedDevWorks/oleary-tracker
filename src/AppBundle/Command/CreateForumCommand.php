<?php

namespace AppBundle\Command;

use AppBundle\Entity\Forum;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateForumCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:create-forum')
            ->setDescription('Create Forum')
            ->addArgument('title', InputArgument::REQUIRED, 'The title of the forum.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Forum Creator',
            '============',
            '',
        ]);

        // retrieve the argument value using getArgument()
        $output->writeln('Title: '.$input->getArgument('email'));

        $forum = new Forum();
        $forum->setTitle($input->getArgument('title'))
              ->setDescription('')
              ->setDateAdded(new \DateTime())
              ->setDateUpdated(new \DateTime());

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($forum);
        $em->flush();
    }
}
