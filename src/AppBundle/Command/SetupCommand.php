<?php

namespace AppBundle\Command;

use AppBundle\Entity\Forum;
use AppBundle\Entity\PromiseCategory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetupCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:setup')
            ->setDescription('Sets up initial database stuff');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Setup',
            '============',
            '',
        ]);
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $output->writeln('Creating Categories');
        $em->persist((new PromiseCategory())->setTitle('Culture')
                                            ->setIcon('culture.png')
                                            ->setDateAdded(new \DateTime())
                                            ->setDateUpdated(new \DateTime()));
        $em->persist((new PromiseCategory())->setTitle('Economy')
                                            ->setIcon('economy.png')
                                            ->setDateAdded(new \DateTime())
                                            ->setDateUpdated(new \DateTime()));
        $em->persist((new PromiseCategory())->setTitle('Environment')
                                            ->setIcon('environment.png')
                                            ->setDateAdded(new \DateTime())
                                            ->setDateUpdated(new \DateTime()));
        $em->persist((new PromiseCategory())->setTitle('Government')
                                            ->setIcon('government.png')
                                            ->setDateAdded(new \DateTime())
                                            ->setDateUpdated(new \DateTime()));
        $em->persist((new PromiseCategory())->setTitle('Immigration')
                                            ->setIcon('immigration.png')
                                            ->setDateAdded(new \DateTime())
                                            ->setDateUpdated(new \DateTime()));
        $em->persist((new PromiseCategory())->setTitle('Security')
                                            ->setIcon('security.png')
                                            ->setDateAdded(new \DateTime())
                                            ->setDateUpdated(new \DateTime()));

        $output->writeln('Creating Forums: ');


        $em->persist((new Forum())->setTitle('General')
                                  ->setSlug('general')
                                  ->setSort(1)
                                  ->setDateAdded(new \DateTime())
                                  ->setDateUpdated(new \DateTime())
                                  ->setDateLastAction(new \DateTime()));
        $em->persist((new Forum())->setTitle('Promises')
                                  ->setSlug('promises')
                                  ->setSort(2)
                                  ->setDateAdded(new \DateTime())
                                  ->setDateUpdated(new \DateTime())
                                  ->setDateLastAction(new \DateTime()));
        $em->persist((new Forum())->setTitle('Off Topic')
                                  ->setSlug('off-topic')
                                  ->setSort(3)
                                  ->setDateAdded(new \DateTime())
                                  ->setDateUpdated(new \DateTime())
                                  ->setDateLastAction(new \DateTime()));
        $em->flush();
    }
}
