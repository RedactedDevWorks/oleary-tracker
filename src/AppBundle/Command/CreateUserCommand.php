<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:create-user')
            ->setDescription('Create User')
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The password of the user.')
            ->addArgument('role', InputArgument::REQUIRED, 'The role of the user.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        // retrieve the argument value using getArgument()
        $output->writeln('Email: '.$input->getArgument('email'));
        $output->writeln('PW: '.$input->getArgument('password'));

//        $userRepo = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Account');
        $user = new Account();
        $user->setEmail($input->getArgument('email'))
             ->setPassword(password_hash($input->getArgument('password'), PASSWORD_BCRYPT))
             ->setDateAdded(new \DateTime())
             ->setRoles([$input->getArgument('role')])
             ->setPreferences([])
             ;

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($user);
        $em->flush();


    }
}
