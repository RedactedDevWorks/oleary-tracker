<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Account;
use Cocur\Slugify\Slugify;

class AppTwigExtensions extends \Twig_Extension
{
    /**
    * @var Slugify
    */
    private $slugify = null;
  
    /**
    * @var Registry
    */
    private $doctrine;

    /**
     * AppTwigExtensions constructor.
     * @param null $slugify
     */
    public function __construct($doctrine)
    {
        $this->slugify = new Slugify();
        $this->doctrine = $doctrine;
    }


    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('gravatar', [$this, 'filter_gravatar']),
            new \Twig_SimpleFilter('base64', 'base64_encode'),
            new \Twig_SimpleFilter('slug', [$this->slugify, 'slugify'])
        ];
    }

    public function getFunctions()
    {
        return [
          new \Twig_SimpleFunction('userRecentFeedCount', [$this, 'recent_user_feed_count'])
        ];
    }

  
    public function recent_user_feed_count(Account $user)
    {
        $since = (new \DateTime())->sub(new \DateInterval('P1D'));
        $count = $this->doctrine->getRepository('AppBundle:RssLog')->getUserFeedCount($user, $since);
        return $count;
    }

    public function filter_gravatar(Account $user, $size = 200) {
        $hash = md5(strtolower(trim($user->getEmail())));
        return "https://www.gravatar.com/avatar/{$hash}?s={$size}";
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_twig_extensions';
    }
}
