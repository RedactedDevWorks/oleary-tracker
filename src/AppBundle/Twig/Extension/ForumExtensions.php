<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Forum;
use AppBundle\Entity\Topic;
use Doctrine\Bundle\DoctrineBundle\Registry;

class ForumExtensions extends \Twig_Extension
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * ForumExtensions constructor.
     * @param $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('count_topics', [$this, 'filter_count_topics']),
            new \Twig_SimpleFilter('count_posts', [$this, 'filter_count_posts']),
            new \Twig_SimpleFilter('count_replies', [$this, 'filter_count_replies'])
        ];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getLastPostSummary', [$this, 'func_get_latest_post_summary']),
            new \Twig_SimpleFunction('getLastReplySummary', [$this, 'func_get_latest_reply_summary'])
        ];
    }

    public function func_get_latest_reply_summary(Topic $topic) {
        return $this->doctrine->getRepository('AppBundle:Topic')->getLatestReplySummary($topic);
    }

    public function func_get_latest_post_summary(Forum $forum) {
        return $this->doctrine->getRepository('AppBundle:Forum')->getLatestPostSummary($forum);
    }

    public function filter_count_replies(Topic $topic) {
        return $this->doctrine->getRepository('AppBundle:Topic')->countReplies($topic);
    }

    public function filter_count_topics(Forum $forum)
    {
        return $this->doctrine->getRepository('AppBundle:Forum')->countTopics($forum);
    }

    public function filter_count_posts(Forum $forum)
    {
        return $this->doctrine->getRepository('AppBundle:Forum')->countPosts($forum);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_forum_extensions';
    }
}
