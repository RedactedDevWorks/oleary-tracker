<?php

namespace AppBundle\Event;

use AppBundle\Entity\Promise;
use Symfony\Component\EventDispatcher\Event;

class PromiseNotificationEvent extends Event
{
    const NAME = 'promise.notification';

    /**
     * @var Promise
     */
    protected $promise;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $source;

    /**
     * PromiseNotificationEvent constructor.
     * @param Promise $promise
     */
    public function __construct(Promise $promise, $status = null, $source = null)
    {
        $this->promise = $promise;
        $this->status = $status;
        $this->source = $source;
    }

    /**
     * @return Promise
     */
    public function getPromise(): Promise
    {
        return $this->promise;
    }

    /**
     * @param Promise $promise
     * @return PromiseNotificationEvent
     */
    public function setPromise(Promise $promise): PromiseNotificationEvent
    {
        $this->promise = $promise;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return PromiseNotificationEvent
     */
    public function setStatus(string $status): PromiseNotificationEvent
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return PromiseNotificationEvent
     */
    public function setSource(string $source): PromiseNotificationEvent
    {
        $this->source = $source;
        return $this;
    }


}