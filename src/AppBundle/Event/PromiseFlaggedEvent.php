<?php

namespace AppBundle\Event;

use AppBundle\Entity\Promise;
use Symfony\Component\EventDispatcher\Event;

class PromiseFlaggedEvent extends Event
{
    const NAME = 'promise.notification';

    /**
     * @var Promise
     */
    protected $promise;

    /**
     * @var \stdClass
     */
    protected $flag;

    /**
     * PromiseNotificationEvent constructor.
     * @param Promise $promise
     */
    public function __construct(Promise $promise, $flag)
    {
        $this->promise = $promise;
        $this->flag = $flag;
    }

    /**
     * @return Promise
     */
    public function getPromise(): Promise
    {
        return $this->promise;
    }

    /**
     * @param Promise $promise
     */
    public function setPromise(Promise $promise)
    {
        $this->promise = $promise;
    }

    /**
     * @return \stdClass
     */
    public function getFlag(): \stdClass
    {
        return $this->flag;
    }

    /**
     * @param string $flag
     */
    public function setFlag(string $flag)
    {
        $this->flag = $flag;
    }
}