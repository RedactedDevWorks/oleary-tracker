<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileSettingsEdit extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email_watched_promises', ChoiceType::class, [
                'choices'  => [
                    'Yes' => true,
                    'No' => false
                ],
                'expanded'=>true,
                'required'=>true])
            ->add('email_watched_topics', ChoiceType::class, [
                'choices'  => [
                    'Yes' => true,
                    'No' => false
                ],
                'expanded'=>true,
                'required'=>true])
            ->add('save', SubmitType::class, array('label' => 'Save'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'app_bundle_profile_settings_edit';
    }
}
