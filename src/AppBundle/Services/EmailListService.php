<?php

namespace AppBundle\Services;


use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;

class EmailListService
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var array
     */
    private $from;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * EmailListService constructor.
     */
    public function __construct(\Swift_Mailer $mailer, array $from, EngineInterface $templating)
    {
        $this->mailer = $mailer;
        $this->from = $from;
        $this->templating = $templating;
    }

    public function send($to, $subject, $template, $params) {
        $email = \Swift_Message::newInstance("[OLearyWatch.com] {$subject}", "", 'text/html')
                                    ->setFrom($this->from['email'], $this->from['name']);

        foreach($to as $subscriber) {
            $subscriber_email = is_string($subscriber) ? $subscriber : $subscriber->getEmail();
            $email->setTo($subscriber_email)
                  ->setBody($this->templating->render($template, array_merge($params, ['recipient'=>$subscriber])));
            $this->mailer->send($email);
        }
    }
}