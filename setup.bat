php composer.phar install
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --dump-sql --force
php bin/console server:run