$(function() {
    $(".flag-toggle").click(function() {
        $(".flagged_promise").val($(this).data('id'));
        $("#flag").modal('show');
    });
    $(".change_title").click(function() {
        $(".chosen_promise").val($(this).data('id'));
        $(".update_title").val($(this).data('value'));
        $("#title").modal('show');
    });
    $(".change_description").click(function() {
        $(".chosen_promise").val($(this).data('id'));
        $(".update_description").text($(this).data('value'));
        $("#description").modal('show');
    });
    $(".change_status").click(function() {
        $(".chosen_promise").val($(this).data('id'));
        $(".chosen_status").val($(this).data('status'));
        $("#status").modal('show');
    });
    $(".view_promise").click(function() {
        $("#promise").load('/modal/promise/'+$(this).data('id')).modal('show');
    });
});